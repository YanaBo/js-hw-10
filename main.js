
$(document).ready(function() {
  const showIcon = 'fa-eye';
  const hideIcon = 'fa-eye-slash';

  $('.icon-password').on('click', function(event) {
    const $input = $($(this).siblings('input')[0]);
    const type = $input.attr('type') === 'text' ? 'password' : 'text';

    let iconType, classToRemove;

    if (type === 'text') {
      iconType = hideIcon;
      classToRemove = showIcon;
    } else {
      iconType = showIcon;
      classToRemove = hideIcon;
    }

    $input.prop('type', type);
    $(this).addClass(iconType).removeClass(classToRemove);
  });

  $('.btn').on('click', function(event) {
    event.preventDefault();
    const password = $('.password').val();
    const confirm = $('.confirm-password').val();
    const $messageBlock = $('.input-wrapper').last();
    const $errorBlock = $('.error-text');

    if (password === confirm) {
      $errorBlock.remove();
      alert('You are welcome!');
    } else {
      if (!$errorBlock.length) {
        $messageBlock.append('<p class="error-text">Нужно ввести одинаковые значения</p>');
      }
    }
  })
});
